use IO::Socket::INET;

# auto-flush on socket
$| = 1;

my $port=7000;

# creating a listening socket
my $socket = new IO::Socket::INET (
#    LocalHost => "10.5.0.58",
    LocalHost => "0.0.0.0",
#    LocalHost => "172.17.0.4",
    LocalPort => $port,
    Proto => tcp,
    Listen => 5,
    Reuse => 1
);
die "cannot create socket $^E $@\n" unless $socket;
print "server waiting for client connection on port $port\n";
my $taskcount=395;

while(1)
{
    # waiting for a new client connection
    my $client_socket = $socket->accept();

    # get information about a newly connected client
    my $client_address = $client_socket->peerhost();
    my $client_port = $client_socket->peerport();
    print "connection from $client_address:$client_port\n";

    # read up to 1024 characters from the connected client
    my $data = "";
    $client_socket->recv($data, 1024);
    chomp($data);
    print "received data: $data sending: $taskcount\n";

    # write response data to the connected client
    $data = "$taskcount";
    $client_socket->send($data);
    $taskcount--;
    # notify client that response has been sent
    shutdown($client_socket, 1);
}

$socket->close();
