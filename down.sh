#!/bin/bash
set -e
env=$(cat .env | cut -d "=" -f 2 | tr '[:upper:]' '[:lower:]')
#sudo docker exec -i -t ${env}_customutil_1  python /app/export_content_packs.py
sudo HTTP_PROXY="squid-sec.internal.ncsa.edu:3128" /usr/local/bin/docker-compose down --remove-orphans
