# -*- coding: utf-8 -*-
'''
# Content-Pack erstellen
api_user=admin
api_user_pw=admin
HOST=localhost
PORT=9000
JQ=/opt/bin/jq-linux64
#curl -u admin:admin -H 'Accept: application/json' -X GET 'http://127.0.0.1:9000/api/system/inputs?pretty=true'
input_ids=$(curl -s -u ${api_user}:${api_user_pw} -H 'Accept: application/json' http://${HOST}:${PORT}/api/system/inputs | ${JQ} '[.inputs[].id]')
output_ids=$(curl -s -u ${api_user}:${api_user_pw} -H 'Accept: application/json' http://${HOST}:${PORT}/api/system/outputs | ${JQ} '[.outputs[].id]')
stream_ids=$(curl -s -u ${api_user}:${api_user_pw} -H 'Accept: application/json' http://${HOST}:${PORT}/api/streams | ${JQ} '[.streams[].id]')
dashboard_ids=$(curl -s -u ${api_user}:${api_user_pw} -H 'Accept: application/json' http://${HOST}:${PORT}/api/dashboards | ${JQ} '[.dashboards[].id]')
grok_ids=$(curl -s -u ${api_user}:${api_user_pw} -H 'Accept: application/json' http://${HOST}:${PORT}/api/system/grok | ${JQ} '[.patterns[].id]')
#echo "inputs ${input_ids}"
#echo "outputs ${output_ids}"
#echo "stram_ids ${stream_id}"
#echo "dashboard_ids ${dashboard_ids}"
#echo "grok_ids ${grok_ids}"

#curl -s -H 'Accept: application/json' -u ${api_user}:${api_user_pw} -X POST http://${HOST}:${PORT}/api/system/bundles/export  name='My-Content-Pack' description='Content-Pack für syslog.xyz.local' category='Backup' inputs:="${input_ids}" streams:="${stream_ids}" outputs:="${output_ids}" dashboards:="${dashboard_ids}" grok_patterns:="${grok_ids}" > contentpack.json

JSON_REQ='{"name":"My-Content-Pack", "description":"", "category":"Backup", "inputs": '${input_ids}' } '
echo ${JSON_REQ}
curl -s -H 'Content-Type: application/json' -H 'Accept: application/json' -u ${api_user}:${api_user_pw} -X POST http://${HOST}:${PORT}/api/system/bundles/export  -d ' "name":"My-Content-Pack", "description":"dumb desc", "category":"Backup", "inputs": '${input_ids}
'''
import os
import json
import requests as rq
from graylog_api_common import *

data = {
    "name": "Master-Content-Pack",
    "description": "saving content pack",
    "category":"Change Control",
}

for k, d in content_pack_items.items():
    resp = rq.get('{base}/{url}'.format(base=base_url, url='/'.join(d['url']+[k])),
                  headers=id_header,
                  auth=auth)
    if resp.status_code != 200:
        raise Exception('API request failed on {item},'
                        ' returned {code}: {mess}'.format(item=k,code=resp.status_code, mess=resp.text))
    ids = [i['id'] for i in resp.json()[(k if d.get('resp_k') is None else d['resp_k'])]]
    data[(k if d.get('data_k') is None else d['data_k'])] = ids

resp = rq.post(export_url, headers=contentpack_header, auth=auth, data=json.dumps(data))

if resp.status_code != 200:
    raise Exception('API request failed on {item},'
                    ' returned {code}: {mess}'.format(item='export', code=resp.status_code, mess=resp.text))

loc_path = os.path.split(os.path.abspath(__file__))[0]
data_dir = os.path.join(loc_path,'data')
contentpack_dir = os.path.join(data_dir,'contentpacks')
if not os.path.exists(data_dir):
    os.makedirs(data_dir)
if not os.path.exists(contentpack_dir):
    os.makedirs(contentpack_dir)


final_dict = resp.json()

# Graylog provides some default content, this removes the default content
# so that there is no confilt durring next restart

with open(os.path.join(data_dir, 'default_from_plugins_contentpack.json')) as f:
    final_dict_default = json.load(f)

common_keys = set(final_dict_default.keys()).intersection(final_dict.keys())
header_keys = {'category', 'name', 'description'}
common_keys = common_keys.difference(header_keys)

for k in common_keys:
    f_def_names = [(i.get('name'), i.get('title')) for i in final_dict_default[k]]
    f_names = [(i.get('name'), i.get('title')) for i in final_dict[k]]
    rm_item = set(f_def_names).intersection(set(f_names))
    final_dict[k] = [i for i in final_dict[k] if (i.get('name'), i.get('title')) not in rm_item]
    final_dict[k] = sorted(final_dict[k], key=lambda x: (x.get('name'), x.get('title')))

#sort lists for consitant output

#for k,v in final_dict.items():
#    if type(v) == list:
#        final_dict[k] = sorted(v, key=lambda x : x.get('title') if x.get('title') is not None else x.get('name') )
contentpack_file=os.path.join(contentpack_dir, 'contentpack.json')
print('Writing to {}'.format(contentpack_file))
with open(contentpack_file, 'wa') as f:
    json.dump(final_dict, f, sort_keys=True, indent=2)

for c_k in common_keys:
    contentpack_file=os.path.join(contentpack_dir, '{}_contentpack.json'.format(c_k))
    print('Writing to {}'.format(contentpack_file))
    break_out_dict = {k: final_dict[k] for k in header_keys.union({c_k})}
    break_out_dict['name'] = '{}_{}'.format(c_k, break_out_dict['name'])
    with open(contentpack_file, 'wa') as f:
        json.dump(break_out_dict, f, sort_keys=True, indent=2)


'''
inputs_ids_url=base_url + "/system/inputs"
outputs_ids_url=base_url + "/system/outputs"
streams_ids_url=base_url + "/system/streams"
dashboards_ids_url=base_url + "/system/dashboards"
grok_ids_url=base_url + "/system/grok"



inputs_resp = rq.get(inputs_ids_url, headers=id_header, auth=auth)
ids = [i['id'] for i in resp.json()['inputs']]
data = {"name":"My-Content-Pack", "description":"dumb desc", "category":"Backup", "inputs": ids}
export_resp = rq.get(export_url, headers=contentpack_header, auth=auth)

'''
