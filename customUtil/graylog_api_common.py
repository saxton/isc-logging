from requests.auth import HTTPBasicAuth
import os
#host = 'localhost'
host = '10.5.0.58'
if 'prod' in os.environ.get('COMPOSE_PROJECT_NAME', '').lower():
    port = '9000'
elif 'staging' in os.environ.get('COMPOSE_PROJECT_NAME', '').lower():
    port = '9001'
elif 'dev' in os.environ.get('COMPOSE_PROJECT_NAME', '').lower():
    port = '9002'
else:
    raise Exception('Can not find the enviroment variable COMPOSE_PROJECT_NAME')

auth=HTTPBasicAuth('admin', 'admin')

id_header = {'Accept' : 'application/json'}
contentpack_header = {'Accept' : 'application/json', 'Content-Type' : 'application/json'}


base_url='http://{host}:{port}/api'.format(host=host, port=port)
export_url=base_url + "/system/bundles/export"
test_url = base_url + '/cluster'
cp_post_url = base_url + '/system/bundles/{bundleId}/apply'
cp_get_url = base_url + '/system/bundles'

content_pack_items = {
    'inputs': {'url': ['system']}, 
    'outputs': {'url': ['system']},
    'streams': {'url': []},
    'dashboards': {'url': []},
    'grok': {'url': ['system'],
             'resp_k': 'patterns',
             'data_k': 'grok_patterns'},
    'lookup/adapters': {'url': ['system'],
                        'resp_k':'data_adapters',
                        'data_k':'lookup_data_adapters'},
    'lookup/caches': {'url': ['system'],
                      'resp_k':'caches',
                      'data_k':'lookup_caches'},
    'lookup/tables': {'url': ['system'],
                      'resp_k': 'lookup_tables',
                      'data_k': 'lookup_tables'}
}
