import requests as rq
from graylog_api_common import *
print('Will use URL {}'.format(test_url))
print('Spin waiting for gralog server to come up')
try:
    resp = rq.get(test_url, headers=id_header, auth=auth)
except rq.ConnectionError:
    conn_error = True
    resp = rq.Response()
else:
    conn_error = False

while str(resp.status_code)[0] is not '2' or conn_error:
    try:
        resp = rq.get(test_url, headers=id_header, auth=auth)
    except rq.ConnectionError:
        conn_error = True
    else:
        conn_error = False

resp = rq.get(cp_get_url, headers=contentpack_header, auth=auth)

cc_data = resp.json()['Change Control']

# load grok paterns, lookup adapter, cache, tables first.
def sort_cp_key(cp_d):
    name = cp_d['name'].lower()
    if 'grok' in name:
        return 0
    elif 'data' in name and 'adapters' in name:
        return 1
    elif 'caches' in name:
        return 2
    elif 'lookup' in name and 'tables' in name:
        return 3
    else:
        return 10000000

cc_ordered_data = sorted(cc_data, key=sort_cp_key)

# remove generic content packs
cc_ordered_data = [d for d in cc_ordered_data if d['name'] != 'Master-Content-Pack']

ordered_ids = [n['id'] for n in cc_ordered_data]
ordered_names = [n['name'] for n in cc_ordered_data]
for i, n in zip(ordered_ids, ordered_names):
    print('Applying content pack {}'.format(n))
    resp = rq.post(cp_post_url.format(bundleId=i), headers=contentpack_header, auth=auth)
    print('Return Status {}'.format(resp.status_code))
    if resp.status_code / 100 != 2:
        print('ERROR! Stopping. message: {}'.format(resp.text))
        break
