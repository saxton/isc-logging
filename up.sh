#!/bin/bash
echo "Starting"
PROD_STAG_OVERRIDE=$(ls | grep '^docker-compose\..*\.yml$' | grep -E 'staging|prod')
NUM_OVER_FOUND=$(echo $PROD_STAG_OVERRIDE | wc -w)
set -e
if [ "${NUM_OVER_FOUND}" -gt "1" ]
then
echo "found multile override options, please remove/rename all but one"
echo ${PROD_STAG_OVERRIDE}
exit 1
elif [ "${NUM_OVER_FOUND}" -eq "1" ]
then
ENV_NAME=$(echo ${PROD_STAG_OVERRIDE} | sed -e 's/docker\-compose\.\(.*\)\.yml/\1/')
echo "Starting ${ENV_NAME}"
cp env_${ENV_NAME} .env
sudo HTTP_PROXY="squid-sec.internal.ncsa.edu:3128" /usr/local/bin/docker-compose -f docker-compose.yml -f ${PROD_STAG_OVERRIDE} up -d
else
echo "Starting dev"
cp env_dev .env
sudo HTTP_PROXY="squid-sec.internal.ncsa.edu:3128" /usr/local/bin/docker-compose up -d
fi

echo "Done"
